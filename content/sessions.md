+++
title = "Sessions"
+++

<section class="section schedule">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-9">
        <div class="row">
          <div class="col">
            <div class="text-left">

## Schedule

### Friday, Oct 18: Workshops

Registration will be open at 8:00AM. Announcements and introductions will be at 8:30AM in the
dining room.

Coffee, tea, soda, and water will be available throughout. Breaks with snacks provided will be at
10:30AM and 3:30PM.

<table>
    <tr>
        <th>Time</th>
        <th>English Room</th>
        <th width="150">Wright Room</th>
        <th>Wedgewood Room</th>
        <th>Dining Room</th>
    </tr>
    <tr>
        <td>9:00AM - 12:00PM</td>
        <td><a href="#intro">Intro to Rust - Jake Goulding</a></td>
        <td><a href="#lisp">Building a Toy Programming Language in Rust - Elias Garcia</a></td>
        <td><a href="#wasm">Building a Plugin System with Rust+Wasm - Robert Masen</a></td>
        <td><a href="#unconf">Unconference - Jean Lange</a></td>
    </tr>
    <tr>
        <td>12:00PM - 2:00PM</td>
        <td colspan="4">Lunch on your own</td>
    </tr>
    <tr>
        <td>2:00PM - 5:00PM</td>
        <td><a href="#blockchain">Write a Blockchain Runtime with Substrate - Joshy Orndorff</a></td>
        <td><a href="#lisp">Building a Toy Programming Language in Rust - Elias Garcia</a></td>
        <td><a href="#lindenmayer">Lindenmayer Systems: Modeling the Natural World - Daan von Berkel & Phil Fried</a></td>
        <td><a href="#unconf">Unconference - Jean Lange</a></td>
    </tr>
</table>


### Saturday, Oct 19: Talks

All Saturday activities will be in the Auditorium.

Registration will be open at 9:00AM. Announcements will be at 9:15AM.

Coffee, tea, soda, and water will be available throughout. Snack breaks will be at 10:30AM and
3:00PM.

<table>
    <tr>
        <th>Time</th>
        <th>Talk</th>
    </tr>
    <tr>
        <td>9:30AM</td>
        <td><a href="#unicode">Unicode in Rust - Illustrated by Kanji - Jenny Manning</a></td>
    </tr>
    <tr>
        <td>10:00AM</td>
        <td><a href="#server">Tips for Writing a Web Server and Beyond - Apoorv Kothari</a></td>
    </tr>
    <tr>
        <td>10:30AM</td>
        <td>Break with snacks provided</td>
    </tr>
    <tr>
        <td>11:00AM</td>
        <td><a href="#ide">Are we *actually* IDE yet? A look on the Rust IDE Story - Igor Matuszewski</a></td>
    </tr>
    <tr>
        <td>11:30AM</td>
        <td><a href="#polonius">Polonius: Either Borrower or Lender Be, but Responsibly - Niko Matsakis</a></td>
    </tr>
    <tr>
        <td>12:00PM</td>
        <td>Lunch on your own</td>
    </tr>
    <tr>
        <td>2:00PM</td>
        <td><a href="#lightning">Lightning Talks - Maybe you!</a></td>
    </tr>
    <tr>
        <td>2:30PM</td>
        <td><a href="#legacy">Introducing Rust into a Legacy Embedded System - Steven Walter</a></td>
    </tr>
    <tr>
        <td>3:00PM</td>
        <td>Break with Snacks Provided</td>
    </tr>
    <tr>
        <td>3:30PM</td>
        <td><a href="#typetheory">Type Theory for the Working Rustacean - Dan Pittman</a></td>
    </tr>
    <tr>
        <td>4:00PM</td>
        <td><a href="#docsrs">The Death and Rebirth of Docs.rs - Quiet Misdreavus</a></td>
    </tr>
    <tr>
        <td>4:30PM</td>
        <td>Raffle drawings, closing announcements</td>
    </tr>
    <tr>
        <td>5:00PM</td>
        <td>RBR 2019 is over! 😭❤️</td>
    </tr>
</table>

</div>
</div>
</div>
</div>
</div>
</div>
</section>

{{ sessions() }}
