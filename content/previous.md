+++
title = "Previous conferences"
+++

<a id="2018"></a>
{% text_section(title = "") %}
# 2018
## Ann Arbor, MI
### October 19 & 20

<iframe width="560" height="315" src="https://www.youtube.com/embed/C4jQbc1RJPY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Videos](https://www.youtube.com/watch?v=C4jQbc1RJPY&list=PLgC1L0fKd7UlpVTHVfLYVtudVx8CzbSxW)
- [Conference website](http://conf2018.rust-belt-rust.com/)
{% end %}

<a id="2017"></a>
{% text_section(title = "") %}
# 2017
## Columbus, OH
### October 26 & 27

<iframe width="560" height="315" src="https://www.youtube.com/embed/l1csV8WiDRQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Videos](https://www.youtube.com/watch?v=l1csV8WiDRQ&list=PLgC1L0fKd7Ul71lD_cImGuMxsZ6J8fa06)
- [Conference website](http://conf2017.rust-belt-rust.com/)
{% end %}

<a id="2016"></a>
{% text_section(title = "") %}
# 2016
## Pittsburgh, PA
### October 27 & 28

<iframe width="560" height="315" src="https://www.youtube.com/embed/-XVIKSm8Rwg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- [Videos](https://www.youtube.com/watch?v=-XVIKSm8Rwg&list=PLgC1L0fKd7UmdG82JOEE0uzXci1XY61xU)
- [Conference website](http://conf2016.rust-belt-rust.com/)
{% end %}
