+++
title = "Code of conduct"
+++

{% text_section(title = "Contact an organizer") %}

* Email: [organizers@rust-belt-rust.com](mailto:organizers@rust-belt-rust.com)
* Twitter: [@rustbeltrust](https://twitter.com/rustbeltrust)
* Slack: [Join here](https://rust-belt-rust-slack.herokuapp.com) and see the #ask-an-organizer channel to quickly find an organizer.
* Phone/text: +1-412-423-8664.
* In person: Organizers will be wearing a different color conference t-shirt.

{% end %}

{% text_section(title = "Emergency contact information") %}

* Medical emergency: Call 911
* Local law enforcement: Dayton Police Department - call 911
* Sexual assault hotline: [YWCA Dayton](https://www.ywcadayton.org/?page_id=5539) +1-937-222-7233
* Local taxi companies: [Lyft](https://www.lyft.com/), [Uber](https://www.uber.com/)

{% end %}

{% text_section(title = "Policy") %}

Rust Belt Rust is dedicated to providing a harassment-free conference experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, age or religion. We do not tolerate harassment of conference participants in any form. Sexual language and imagery is not appropriate for any conference venue, including talks. Conference participants violating these rules may be sanctioned or expelled from the conference without a refund at the discretion of the conference organizers.

Harassment includes, but is not limited to:

* Verbal comments that reinforce social structures of domination related to gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, age, or religion.
* Sexual images in public spaces
* Deliberate intimidation, stalking, or following
* Harassing photography or recording
* Sustained disruption of talks or other events
* Inappropriate physical contact
* Unwelcome sexual attention
* Advocating for, or encouraging, any of the above behavior

{% end %}

{% text_section(title = "Enforcement") %}

Participants asked to stop any harassing behavior are expected to comply immediately. Exhibitors in the expo hall, sponsor or vendor booths, or similar activities are also subject to the anti-harassment policy. In particular, exhibitors should not use sexualized images, activities, or other material. Booth staff (including volunteers) should not use sexualized clothing/uniforms/costumes, or otherwise create a sexualized environment.

If a participant engages in harassing behavior, event organizers retain the right to take any actions to keep the event a welcoming environment for all participants. This includes warning the offender or expulsion from the conference with no refund.

Event organizers may take action to redress anything designed to, or with the clear impact of, disrupting the event or making the environment hostile for any participants.

We expect participants to follow these rules at all event venues and event-related social activities. We think people should follow these rules outside event activities too!

{% end %}

{% text_section(title = "Reporting") %}

If someone makes you or anyone else feel unsafe or unwelcome, please report it as soon as possible. Conference staff can be identified by their t-shirts and special badges. Harassment and other code of conduct violations reduce the value of our event for everyone. We want you to be happy at our event. People like you make our event a better place.

You can make a personal report by:

* Calling or messaging this phone number: +1-412-423-8664. This phone number will be continuously monitored for the duration of the event.
* Contacting a staff member, identified by STAFF badges, buttons, or shirts.

When taking a personal report, our staff will ensure you are safe and cannot be overheard. They may involve other event staff to ensure your report is managed properly. Once safe, we'll ask you to tell us about what happened. This can be upsetting, but we'll handle it as respectfully as possible, and you can bring someone to support you. You won't be asked to confront anyone and we won't tell anyone who you are.

Our team will be happy to help you contact hotel/venue security, local law enforcement, local support services, provide escorts, or otherwise assist you to feel safe for the duration of the event. We value your attendance.

{% end %}

{% text_section(title = "Credit") %}

This anti-harassment policy is based on the example policy from the [Geek Feminism wiki](http://geekfeminism.wikia.com/wiki/Conference_anti-harassment), created by the Ada Initiative and other volunteers.

{% end %}
