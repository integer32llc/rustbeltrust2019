+++
title = "Sponsors"
+++

<section class="section">
<div class="container">

{% sponsor_level(level = "Gold Sponsors") %}
  <a href="https://www.mozilla.org/">
    <img class="sponsor__logo" src="/images/sponsors/mozilla.svg" alt="Mozilla"/>
    <h3>Mozilla</h3>
  </a>
{% end %}
{% sponsor_level(level = "Silver Sponsors") %}
  <a href="https://www.imperva.com/">
    <img class="sponsor__logo" src="/images/sponsors/imperva.svg" alt="Imperva"/>
    <h3>Imperva</h3>
  </a>
{% end %}
{% sponsor_level(level = "Bronze Sponsors") %}
  <a href="https://www.coffeeandcode.com/">
    <img class="sponsor__logo" src="/images/sponsors/coffee-and-code.png" alt="Coffee and Code"/>
    <h3>Coffee and Code</h3>
  </a>
  <a href="https://www.10xgenomics.com/">
    <img class="sponsor__logo" src="/images/sponsors/10x-genomics.svg" alt="10x Genomics"/>
    <h3>10x Genomics</h3>
  </a>
{% end %}

{% sponsor_level(level = "Speaker Sponsors") %}
  <a href="https://www.lexmark.com/">
    <img class="sponsor__logo" src="/images/sponsors/lexmark.svg" alt="Lexmark"/>
    <h3>Lexmark</h3>
  </a>
{% end %}

{% sponsor_level(level = "Infrastructure") %}
  <a href="https://integer32.com">
    <img class="sponsor__logo" src="/images/sponsors/integer32.png" alt="Integer 32"/>
    <h3>Integer 32</h3>
  </a>
{% end %}

{% sponsor_level(level = "Prize") %}
  <a href="https://nostarch.com/">
    <img class="sponsor__logo" src="/images/sponsors/no_starch_press.png" alt="No Starch Press"/>
    <h3>No Starch Press</h3>
  </a>
{% end %}

{% sponsor_level(level = "Logo Design") %}
  <a href="https://littlelines.com/">
    <img class="sponsor__logo" src="/images/sponsors/littlelines.png" alt="Littlelines"/>
    <h3>Littlelines</h3>
  </a>
{% end %}

</div>
</section>

{% text_section(title="Interested in sponsoring?") %}

Please [download our
prospectus](/rust-belt-rust-2019-sponsorship-prospectus.pdf). If you
have any questions, please email
[sponsorship@rust-belt-rust.com](mailto:sponsorship@rust-belt-rust.com).

{% end %}
